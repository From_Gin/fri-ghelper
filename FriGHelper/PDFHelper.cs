﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IronPdf;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
//using iTextSharp.text.pdf;
//using iTextSharp.text;
//using iTextSharp.tool.xml;
using Spire.Pdf;
using Spire.Doc;
using Section = Spire.Doc.Section;
/// <summary>
/// 使用IronPdf组件
/// 官网地址 https://ironpdf.com/
/// </summary>
namespace FriGHelper
{
    public class PDFHelper
    {
        //获取文件存储地址
        string url = ConfigurationManager.AppSettings["PdfUrl"];
        //根据配置路径
        public string CreatePDF(string HtmlInput,string SaveUrl)
        {
            //var Renderer = new IronPdf.HtmlToPdf();
            //IronPdf.ChromePdfRenderer Renderer = new IronPdf.ChromePdfRenderer();
            //var PDF = Renderer.RenderHtmlAsPdf(HtmlInput);
            //PDF.SaveAs(url+SaveUrl);
            //url = url + SaveUrl;
            //return url;
            //byte[] pdfFile = ConvertHtmlTextToPDF(HtmlInput);

            //System.IO.File.WriteAllBytes(SaveUrl, pdfFile);
            CreatePDFAspose(HtmlInput, SaveUrl);
            url = url + SaveUrl;
            return url;
        }

        //存储路径根据获得DIRURL来存储
        public string CreatePDF(string HtmlInput, string SaveUrl,string DirUrl)
        {
            //用Iron方法
            //var Renderer = new IronPdf.HtmlToPdf();            
            //IronPdf.ChromePdfRenderer Renderer = new IronPdf.ChromePdfRenderer();
            //var PDF = Renderer.RenderHtmlAsPdf(HtmlInput);            
            // PDF.SaveAs(DirUrl + SaveUrl);
            CreatePDFAspose(HtmlInput, SaveUrl);
            //Document document = new Document();
            //Section sec = document.AddSection();
            //sec.AddParagraph().AppendHTML(HtmlInput);
            //document.SaveToFile("HtmlstringtoPDF.PDF", Spire.Doc.FileFormat.PDF);
            url = DirUrl + SaveUrl;
            return url;
            //byte[] pdfFile = ConvertHtmlTextToPDF(HtmlInput);
            //System.IO.File.WriteAllBytes(SaveUrl, pdfFile);
        }

        //public byte[] ConvertHtmlTextToPDF(string htmlText)
        //{
        //    htmlText = "<p>" + htmlText + "</p>";
        //    MemoryStream outputStream = new MemoryStream();//要把PDF写到哪个串流

        //    byte[] data = Encoding.UTF8.GetBytes(htmlText);//字串转成byte[]
        //    MemoryStream msInput = new MemoryStream(data);
        //    Document doc = new Document();//要写PDF的文件，建构子没填的话预设直式A4
        //    PdfWriter writer = PdfWriter.GetInstance(doc, outputStream);
        //    //指定文件预设开档时的缩放为100%

        //    PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, doc.PageSize.Height, 1f);
        //    //开启Document文件 
        //    doc.Open();

        //    //使用XMLWorkerHelper把Html parse到PDF档里
        //    // XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msInput, null, Encoding.UTF8, new UnicodeFontFactory());
        //    XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msInput, null, Encoding.UTF8);

        //    //将pdfDest设定的资料写到PDF档
        //    PdfAction action = PdfAction.GotoLocalPage(1, pdfDest, writer);
        //    writer.SetOpenAction(action);
        //    doc.Close();
        //    msInput.Close();
        //    outputStream.Close();
        //    //回传PDF档案 
        //    return outputStream.ToArray();
        //}
    
        //AposePDF打印
        public void CreatePDFAspose(string HtmlInput, string SaveUrl)
        {
            Aspose.Pdf.Document doc = new Aspose.Pdf.Document();
            Aspose.Pdf.Page page = doc.Pages.Add();
            page.Paragraphs.Add(new Aspose.Pdf.Text.TextFragment(HtmlInput));
            doc.Save(@"d:\3.pdf");
            Console.WriteLine("ok");
            Console.ReadLine();
        }
    
    }
}
